package com.itsmeccr.emergencynepal;

import com.itsmeccr.models.PrivateHelp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

public class AddPrivateHelp extends Activity {
private String name,address,email,mobile,relation;
private PrivateHelp privateHelp;
private SQLiteDatabase db=null;
private DataBaseHelper databaseHelper;
private Intent intent;
private PrivateHelp oldHelp;
@Override
protected void onDestroy() {
	// TODO Auto-generated method stub
	if(db!=null)
		db.close();
	databaseHelper.close();
	super.onDestroy();
}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		intent=getIntent();
		
				setContentView(R.layout.private_help_add);
				
				databaseHelper=new DataBaseHelper(getApplicationContext());
				db=databaseHelper.openDataBase();
				final Button okButton=(Button)findViewById(R.id.btnOK);
				final EditText editName=(EditText)findViewById(R.id.editName);
				final EditText editAddress=(EditText)findViewById(R.id.editAddress);
				final EditText editPhone=(EditText)findViewById(R.id.editPhone);
				final EditText editEmail=(EditText)findViewById(R.id.editEmail);
				okButton.setEnabled(false);
				final MultiAutoCompleteTextView editRelation=(MultiAutoCompleteTextView)findViewById(R.id.editRelation);
				if(intent.getStringExtra("Task").equals("edit")){
					setTitle("Edit Private Help");
					okButton.setEnabled(true);
				}
				Bundle bundle=intent.getExtras();
				oldHelp=new PrivateHelp();
				oldHelp=oldHelp.fromBundle(bundle);
				editName.setText(oldHelp.getName());
				editAddress.setText(oldHelp.getAddress());
				editPhone.setText(oldHelp.getMobile());
				editEmail.setText(oldHelp.getEmail());
				editRelation.setText(oldHelp.getRelationship());
				editRelation.setOnFocusChangeListener(new OnFocusChangeListener() {
					
					@Override
					public void onFocusChange(View arg0, boolean arg1) {
						// TODO Auto-generated method stub
						if(arg1){
						name=editName.getText().toString();
						address=editAddress.getText().toString();
						mobile=editPhone.getText().toString();
						email=editEmail.getText().toString();
						
						if(name.length()!=0&&address.length()!=0&&mobile.length()!=0){
							
							//Toast.makeText(getApplicationContext(),privateHelp.toString(), Toast.LENGTH_LONG).show();
						okButton.setEnabled(true);
						}
						}
					}
				});
				okButton.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						//Log.d("CCR", "Reached inside onClick");
						name=editName.getText().toString();
						address=editAddress.getText().toString();
						mobile=editPhone.getText().toString();
						email=editEmail.getText().toString();
					relation=editRelation.getText().toString();
					//Log.d("CCR", "Relation="+relation);
					//Toast.makeText(getApplicationContext(),relation, Toast.LENGTH_LONG).show();
					if(relation.length()!=0){
						//Log.d("CCR", "Reached inside Condition");
						privateHelp=new PrivateHelp();
						Log.d("CCR", "Reached inside Condition");
						try{
						if (privateHelp.setMobile(mobile)){
							Log.d("CCR", "Reached inside try");
							//db.execSQL("delete from privatehelp where 1");]
							
							String task=intent.getStringExtra("Task");
							if(task.equals("add")){
								
							SQLiteStatement stm=db.compileStatement("insert into privatehelp(name,address,mobile,email,relationship) values(?,?,?,?,?)");
						stm.bindAllArgsAsStrings(new String[]{name,address,mobile,email,relation});
						stm.execute();
						Toast.makeText(getApplicationContext(),"Contact Added", Toast.LENGTH_LONG).show();
						finish();
							}
							else if(task.equals("edit")){
								
								int id=intent.getIntExtra("position",-1);
								//Toast.makeText(getApplicationContext(),"Position="+id, Toast.LENGTH_LONG).show();
							SQLiteStatement stm=db.compileStatement("update privatehelp set name=?,address=?,mobile=?,email=?,relationship=? where " +
									"name=? and address=? and mobile=? and email=? and relationship=?");
							String[] args=new String[]{name,address,mobile,email,relation,oldHelp.getName(),oldHelp.getAddress(),oldHelp.getMobile(),oldHelp.getEmail(),oldHelp.getRelationship()};
							stm.bindAllArgsAsStrings(args);
							stm.execute();
							Toast.makeText(getApplicationContext(),"Edit Success", Toast.LENGTH_LONG).show();
							//startActivity(new Intent(getApplicationContext(), PrivateHelpList.class));
							finish();
							}
						}else
							Toast.makeText(getApplicationContext(),"Please input Valid Mobile and Email", Toast.LENGTH_LONG).show();
						}catch(SQLException e){
						Toast.makeText(getApplicationContext(), "Error Adding ", Toast.LENGTH_LONG).show();	
						}	
					}
					}
				});
				Button cancelButton=(Button)findViewById(R.id.btncancel);
				cancelButton.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						finish();
					}
				});
				

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_configure_private_help, menu);
		return true;
	}

}
