package com.itsmeccr.emergencynepal;

import java.util.ArrayList;
import java.util.List;

import com.itsmeccr.models.District;
import com.itsmeccr.models.PoliceStation;
import com.itsmeccr.models.ManualAddress;
import com.itsmeccr.models.Zone;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class PoliceStationList extends ListActivity {
	List<PoliceStation> policeStations=new ArrayList<PoliceStation>();
	SQLiteDatabase db=null;
	DataBaseHelper databaseHelper;
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if(db!=null)
			db.close();
		databaseHelper.close();
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
		policeStations=new ArrayList<PoliceStation>();
		//db.execSQL("delete from policeStation where phone='234567'");
		Cursor curs=db.rawQuery("select * from policestation",null);
		//String count1=String.valueOf(curs.getCount());
		//Toast.makeText(this,count1, Toast.LENGTH_LONG).show();
		if(curs.moveToFirst()){
			
		
		do{
				ManualAddress address=new ManualAddress();
				PoliceStation policeStation=new PoliceStation();
				String name=curs.getString(curs.getColumnIndex("name"));
				policeStation.setName(name);
				String phone=curs.getString(curs.getColumnIndex("mobile"));
				policeStation.setPhone(phone);
				phone=curs.getString(curs.getColumnIndex("phone"));
				policeStation.setPhone(phone);
				String district=curs.getString(curs.getColumnIndex("district"));
				District dist=District.valueOf(district);
				address.setDistrict(dist);
				address.setZone(Zone.valueOf(dist.getZone()));
				String mun=curs.getString(curs.getColumnIndex("mun"));
				String locality=curs.getString(curs.getColumnIndex("locality"));
				address.setMun(mun);
				address.setLocality(locality);
				policeStation.setAddress(address);
				policeStations.add(policeStation);
		}while(curs.moveToNext());
		}
		 final List<String> ACTIVITY_CHOICES=new ArrayList<String>();
			for(PoliceStation policeStation:policeStations)
			ACTIVITY_CHOICES.add(policeStation.toString());
			/*setListAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, ACTIVITY_CHOICES));*/
			setListAdapter(new MyListAdapdter(this,ACTIVITY_CHOICES));
					getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
					getListView().setTextFilterEnabled(true);
					getListView().setOnItemClickListener(new OnItemClickListener()
					{
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
						 final List<String> phones=policeStations.get(arg2).getPhone();
						 AlertDialog dialog = new AlertDialog.Builder(PoliceStationList.this).create();
							dialog.setMessage("Please Choose Phone Number");
							dialog.setButton(DialogInterface.BUTTON_NEUTRAL, phones.get(0), new DialogInterface.OnClickListener() {							
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									try{
										Intent callIntent=new Intent(Intent.ACTION_CALL);
										callIntent.setData(Uri.parse("tel:"+phones.get(0)));
										startActivity(callIntent);
									}catch(ActivityNotFoundException e){
										Log.e("Calling a Phone Number","Call failed",e);
									}
									
								}
							});
							if(phones.size()==2)
							dialog.setButton(DialogInterface.BUTTON_POSITIVE, phones.get(1), new DialogInterface.OnClickListener() {							
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									try{
										Intent callIntent=new Intent(Intent.ACTION_CALL);
										callIntent.setData(Uri.parse("tel:"+phones.get(1)));
										startActivity(callIntent);
									}catch(ActivityNotFoundException e){
										Log.e("Calling a Phone Number","Call failed",e);
									}
									
								}
							});
							
							dialog.show();
						
								
					}
					});
		
		
	}

}
