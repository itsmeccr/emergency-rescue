package com.itsmeccr.emergencynepal;

import java.util.ArrayList;
import java.util.List;

import com.itsmeccr.models.District;
import com.itsmeccr.models.Hospital;
import com.itsmeccr.models.ManualAddress;
import com.itsmeccr.models.PrivateHelp;
import com.itsmeccr.models.Zone;

import android.R.anim;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PrivateHelpList extends Activity {
List<String> helpList=new ArrayList<String>();
static final int CODE_ADD=1001;
static final int CODE_EDIT=1010;
static final int CODE_DELETE=1011;
//static final int CODE_RESULT_OK=0001;
ListView listView;
private List<PrivateHelp> privateHelps=new ArrayList<PrivateHelp>();
private SQLiteDatabase db=null;
private DataBaseHelper databaseHelper;
private MyListAdapdter myListAdapdter;
@Override
protected void onDestroy() {
	// TODO Auto-generated method stub
	if(db!=null)
		db.close();
	databaseHelper.close();
	super.onDestroy();
}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_private_help);
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
				
	}
	Cursor curs;
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		
		super.onStart();
		Button delete=(Button)findViewById(R.id.btndelete);
		Button edit=(Button)findViewById(R.id.btnedit);
		ImageButton sendSms=(ImageButton)findViewById(R.id.btnSendSms);
		TextView notify=(TextView)findViewById(R.id.notify);
		TextView smsText=(TextView)findViewById(R.id.textsms);
		//Toast.makeText(getApplicationContext(),"onStart", Toast.LENGTH_LONG).show();
		privateHelps=new ArrayList<PrivateHelp>();
		helpList.clear();
		try{
			curs=db.rawQuery("select * from privatehelp",null);
			
			//String count1=String.valueOf(curs.getCount());
			//Toast.makeText(this,count1, Toast.LENGTH_LONG).show();
			if(curs.getCount()==0){
				delete.setVisibility(Button.GONE);
				edit.setVisibility(Button.GONE);
				sendSms.setVisibility(Button.GONE);
				notify.setVisibility(View.VISIBLE);
				smsText.setVisibility(Button.GONE);
			}
			else
			{
				delete.setVisibility(Button.VISIBLE);
				edit.setVisibility(Button.VISIBLE);
				sendSms.setVisibility(Button.VISIBLE);
				notify.setVisibility(View.GONE);
				smsText.setVisibility(Button.VISIBLE);
			}
			if(curs.moveToFirst()){
			do{
					PrivateHelp privateHelp=new PrivateHelp();
					String name=curs.getString(curs.getColumnIndex("name"));
					privateHelp.setName(name);
					String mobile=curs.getString(curs.getColumnIndex("mobile"));
					privateHelp.setMobile(mobile);
					String address=curs.getString(curs.getColumnIndex("address"));
					privateHelp.setAddress(address);
					String email=curs.getString(curs.getColumnIndex("email"));
					privateHelp.setEmail(email);
					String relationship=curs.getString(curs.getColumnIndex("relationship"));
					privateHelp.setRelationship(relationship);
					privateHelps.add(privateHelp);
			}while(curs.moveToNext());
			}
			}catch (SQLException e) {
				// TODO: handle exception
				Log.d("CCR", "SQL ERROR");
			}
		for(PrivateHelp privateHelp:privateHelps){
			helpList.add(privateHelp.toString());
		}
		myListAdapdter=new MyListAdapdter(this,helpList);
		listView=(ListView)findViewById(R.id.listView1);
		listView.setAdapter(myListAdapdter);
		listView.setTextFilterEnabled(true);
		listView.setBackgroundColor(Color.GRAY);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Clicked on Item "+pos, Toast.LENGTH_LONG).show();
				final String mobile=privateHelps.get(pos).getMobile();
				AlertDialog dialog = new AlertDialog.Builder(PrivateHelpList.this).create();
				dialog.setMessage("Please Choose What to do?");
				dialog.setButton(DialogInterface.BUTTON_NEUTRAL,"Call", new DialogInterface.OnClickListener() {							
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						try{
							Intent callIntent=new Intent(Intent.ACTION_CALL);
							callIntent.setData(Uri.parse("tel:"+mobile));
							startActivity(callIntent);
						}catch(ActivityNotFoundException e){
							Log.e("Calling a Phone Number","Call failed",e);
						}
					}
				});
				dialog.setButton(DialogInterface.BUTTON_POSITIVE,"SMS", new DialogInterface.OnClickListener() {							
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), "Sending Sms", Toast.LENGTH_LONG).show();
						sendSms(mobile);
					}
				});
				
				dialog.show();
				
			}
		});
		Button add=(Button)findViewById(R.id.btnadd);
		if(curs.getCount()==3){
			add.setEnabled(false);
		}
		else
			add.setEnabled(true);
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(PrivateHelpList.this,AddPrivateHelp.class);
				intent.putExtra("Task", "add");
				startActivityForResult(intent, CODE_ADD);
				
			}
		});

delete.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
	Intent intent=new Intent(PrivateHelpList.this,DeletePrivateHelp.class);
	startActivity(intent);
	}
});
edit.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
	Intent intent=new Intent(PrivateHelpList.this,EditPrivateHelp.class);
	intent.putExtra("Task", "edit");
	startActivityForResult(intent, CODE_EDIT);
	}
});

sendSms.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		AlertDialog dialog = new AlertDialog.Builder(PrivateHelpList.this).create();
		dialog.setMessage("Are you Sure you want to Send Sms to all Contacts. You will be Charged by your Network Provider");
		dialog.setButton(DialogInterface.BUTTON_POSITIVE,"Confirm", new DialogInterface.OnClickListener() {							
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				for(PrivateHelp help:privateHelps){
					sendSms(help.getMobile());
				}
			}
		});
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener() {							
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
		});
		
		dialog.show();
		
		
	}
});
		//myListAdapdter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_private_help, menu);
		return true;
	}
	
	void sendSms(String mobile){
		String SENT_SMS_FLAG = "SENT_SMS";
		String DELIVER_SMS_FLAG = "DELIVER_SMS";
		SmsManager mySms=SmsManager.getDefault();
		SharedPreferences prefs=getSharedPreferences("myAddressPreference",Context.MODE_PRIVATE);
		
		 String zone=prefs.getString("zone", "");
		
		 String district=prefs.getString("district", "");
		 
		 String mun=prefs.getString("mun", "");
		
		 String locality=prefs.getString("locality", "");
		ManualAddress userAddress=new ManualAddress();
		userAddress.setZone(Zone.valueOf(zone));
		userAddress.setDistrict(District.valueOf(district));
		userAddress.setMun(mun);
		userAddress.setLocality(locality);
		String msg="Message From Emergency Response App:" +
				"The Sender may be in emergency or trouble.Please Respond." +
				"Location:"+userAddress.toString();
		Intent sentIn = new Intent(SENT_SMS_FLAG);
		PendingIntent sentPIn = PendingIntent.getBroadcast(this,0,sentIn,0);
		Intent deliverIn = new Intent(DELIVER_SMS_FLAG);
		PendingIntent deliverPIn
		= PendingIntent.getBroadcast(this,0,deliverIn,0);
		ArrayList<String> multiSMS = mySms.divideMessage(msg);
		ArrayList<PendingIntent> sentIns = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> deliverIns = new ArrayList<PendingIntent>();
		for(int i=0; i< multiSMS.size(); i++){
		sentIns.add(sentPIn);
		deliverIns.add(deliverPIn);
		}
		mySms.sendMultipartTextMessage(mobile, null,
		multiSMS, sentIns, deliverIns);
		BroadcastReceiver sentReceiver = new BroadcastReceiver(){
			@Override public void onReceive(Context c, Intent in) {
			switch(getResultCode()){
			case Activity.RESULT_OK:
			//sent SMS message successfully;
				Toast.makeText(c, "SMS sent Successfully", Toast.LENGTH_LONG).show();
			break;
			default:
			//sent SMS message failed
				Toast.makeText(c, "Failed to send SMS", Toast.LENGTH_LONG).show();
			break;
			}
			}
			};
			BroadcastReceiver deliverReceiver = new BroadcastReceiver(){
			@Override public void onReceive(Context c, Intent in) {
			//SMS delivered actions
				Toast.makeText(c, "SMS Delivered", Toast.LENGTH_LONG).show();
			}
			};
			registerReceiver(sentReceiver, new IntentFilter(SENT_SMS_FLAG));
			registerReceiver(deliverReceiver, new IntentFilter(DELIVER_SMS_FLAG));
	}

}

