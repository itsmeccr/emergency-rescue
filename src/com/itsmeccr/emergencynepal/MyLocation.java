package com.itsmeccr.emergencynepal;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.itsmeccr.models.District;
import com.itsmeccr.models.GeoLocation;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MyLocation extends Activity {
LocationManager mLocationManager;
TextView tv;
Location mLocation;
private SQLiteDatabase db=null;
private DataBaseHelper databaseHelper;
List<Address> longLat;
GeoLocation location1,location2;
protected void onDestroy() {
	// TODO Auto-generated method stub
	if(db!=null)
		db.close();
	databaseHelper.close();
	super.onDestroy();
}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_location);
		Intent i=getIntent();
		final String district=i.getStringExtra("district");
		final String zone=i.getStringExtra("zone");
		tv=(TextView)findViewById(R.id.myLocation);
		mLocationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria=new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(criteria.POWER_LOW);
		String locationProvider=mLocationManager.getBestProvider(criteria, true);
		//List<String> providers = mLocationManager.getAllProviders();
		List<Address> addresses;
		//tv.setText(providers.toString());
		StringBuilder mSB = new StringBuilder("Providers:\n");
		mLocationManager.requestLocationUpdates(
		locationProvider, 5000, 500.0f, new LocationListener(){
		// these methods are required
		public void onLocationChanged(Location location) {
		Toast.makeText(getApplicationContext(), "Location Changed", Toast.LENGTH_LONG).show();	
		}
		public void onProviderDisabled(String arg0) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String a, int b, Bundle c) {}
		});
		mSB.append(locationProvider).append(": \n");
		mLocation =
		mLocationManager.getLastKnownLocation(locationProvider);
		if(mLocation != null) {
		mSB.append(mLocation.getLatitude()).append(" , ");
		mSB.append(mLocation.getLongitude()).append("\n");
		////////////////For getting distance in space/////////////////////
		location1=new GeoLocation();
		location1.setLatitude(mLocation.getLatitude());
		location1.setLongitude(mLocation.getLongitude());
		location1.setZone("zone");
		location1.setDistrict("district");
		/////////////////////////////////////////////////////////////////
		Geocoder mGC = new Geocoder(this, Locale.ENGLISH);
		try {
			tv.setText(mSB.toString());
			longLat=mGC.getFromLocationName(district+",Nepal",10);
			//////////////////////for getting distance///////////////////////
			location2=new GeoLocation();
			location2.setDistrict(district);
			location2.setZone(zone);
			location2.setLatitude(longLat.get(0).getLatitude());
			location2.setLongitude(longLat.get(0).getLongitude());
			addresses = mGC.getFromLocation(mLocation.getLatitude(),
			mLocation.getLongitude(), 1);
			if(addresses != null) {
				Address currentAddr = addresses.get(0);
				StringBuilder mSB1 = new StringBuilder("Address:\n");
				for(int i1=0; i1<currentAddr.getMaxAddressLineIndex(); i1++) {
				mSB1.append(currentAddr.getAddressLine(i1)).append("\n");
				}
				String adminArea=currentAddr.getAdminArea();
				String subAdminArea=currentAddr.getSubAdminArea();
				String locality=currentAddr.getLocality();
				String subLocality=currentAddr.getSubLocality();
				mSB1.append(adminArea).append("\n").append(subAdminArea).append("\n");
				mSB1.append(locality).append("\n");
				tv.setText(mSB.toString()+mSB1.toString()+"\n"
						+longLat.toString()+"\n"+"Distance: "+location1.getDistance(location2));
				}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
		
		} else {
		mSB.append("Location can not be found\n");
		}
		/*try {
			Geocoder mGC = new Geocoder(this, Locale.ENGLISH);
			addresses = mGC.getFromLocation(mLocation.getLatitude(),
			mLocation.getLongitude(), 1);
			if(addresses != null) {
			Address currentAddr = addresses.get(0);
			StringBuilder mSB1 = new StringBuilder("Address:\n");
			for(int i1=0; i1<currentAddr.getMaxAddressLineIndex(); i1++) {
			mSB1.append(currentAddr.getAddressLine(i1)).append("\n");
			}
			tv.setText(mSB1.toString());
			}
			} catch(IOException e) {
			tv.setText(e.getMessage());
			}*/
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
		Button insert=(Button)findViewById(R.id.btninsert);
		insert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try{
					
				SQLiteStatement stm=db.compileStatement("insert into location(district,latitude,longitude,zone)" +
						"values(?,?,?,?)");
				stm.bindString(1, district);
				
				stm.bindDouble(2,longLat.get(0).getLatitude());
				stm.bindDouble(3,longLat.get(0).getLongitude());
				stm.bindString(4,zone);
				stm.execute();
				Toast.makeText(getApplicationContext(), "Insert Successful", Toast.LENGTH_LONG).show();
				}catch(SQLException e){
					Toast.makeText(getApplicationContext(), "Error inserting:"+e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		});
		}
		

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_my_location, menu);
		return true;
	}

}
