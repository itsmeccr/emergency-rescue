package com.itsmeccr.emergencynepal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import com.itsmeccr.models.District;
import com.itsmeccr.models.GeoLocation;
import com.itsmeccr.models.ManualAddress;
import com.itsmeccr.models.PrivateHelp;
import com.itsmeccr.models.Zone;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

public class SetAddressManually extends Activity {
private String zone,district,mun,locality;	
/////////////////////////////////////////////////////////	
ListView listView;
private SQLiteDatabase db=null;
private DataBaseHelper databaseHelper;
private MyListAdapdter myListAdapdter;
private List<String> locationList=new ArrayList<String>();

private static final Zone[] zones=Zone.values();

@Override
protected void onDestroy() {
	// TODO Auto-generated method stub
	/*if(db!=null)
		db.close();
	databaseHelper.close();*/
	super.onDestroy();
}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_address_manually);
		//zone spinner
		
		//ArrayAdapter to generate zone values
		ArrayAdapter<String> zoneAdapter=new ArrayAdapter<String>(this, R.layout.spinner_entry);
		zoneAdapter.setDropDownViewResource(R.layout.spinner_entry);
		//adding values to the adapter
		for(int i=0;i<zones.length;i++){
			zoneAdapter.add(zones[i].toString());
		}
		//setting the adapter with the values
		Spinner selectZone=(Spinner)findViewById(R.id.spinnerZone);
		selectZone.setAdapter(zoneAdapter);
		selectZone.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long id) {
				// TODO Auto-generated method stub
				
			
				
		//Toast.makeText(SetAddressManually.this, "Position: "+pos+"ID: "+id+arg0.getItemAtPosition(pos).toString(), Toast.LENGTH_LONG).show();
		zone=(String) arg0.getItemAtPosition(pos);
				selectDistrict(zone);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		Spinner selectDistrict=(Spinner)findViewById(R.id.spinnerDistrict);
		selectDistrict.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				district=arg0.getItemAtPosition(arg2).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Button okButton=(Button)findViewById(R.id.button1);
		okButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ManualAddress userAddress=new ManualAddress();
				userAddress.setZone(Zone.valueOf(zone));
				userAddress.setDistrict(District.valueOf(district));
				EditText mun1=(EditText)findViewById(R.id.editText1);
				userAddress.setMun(mun1.getText().toString());
				EditText loc=(EditText)findViewById(R.id.editText2);
				userAddress.setLocality(loc.getText().toString());
				saveAddressPreferences(userAddress);
			}
		});
		Button cancelButton=(Button)findViewById(R.id.button2);
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		Button autoLocation=(Button)findViewById(R.id.autoLocation);
		autoLocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getApplicationContext(),MyLocation.class);
				intent.putExtra("district", district);
				intent.putExtra("zone", zone);
				startActivity(intent);
			}
		});
		/*Button backUp=(Button)findViewById(R.id.btnbackup);
		backUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Clicked", Toast.LENGTH_LONG).show();
				//backUpDatabase();
			}
		});*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_set_address_manually, menu);
		return true;
	}
	protected void selectDistrict(String selectedZone){
		ArrayAdapter<String> distAdapter=new ArrayAdapter<String>(SetAddressManually.this, R.layout.spinner_entry);
		distAdapter.setDropDownViewResource(R.layout.spinner_entry);
		for(District dist:District.values()){
			if(dist.getZone().equals(selectedZone))
			distAdapter.add(dist.name());
		}
		//district Adapter
		Spinner selectDistrict=(Spinner)findViewById(R.id.spinnerDistrict);
		selectDistrict.setAdapter(distAdapter);
	}
	protected void saveAddressPreferences(ManualAddress userAddress) {
		SharedPreferences prefs=getSharedPreferences("myAddressPreference", MODE_PRIVATE);
		Editor mEditor=prefs.edit();
		mEditor.putString("zone", userAddress.getZone().name());
		mEditor.putString("district", userAddress.getDistrict().name());
		mEditor.putString("mun", userAddress.getMun());
		mEditor.putString("locality", userAddress.getLocality());
		mEditor.commit();	
		
		finish();
	}
	private void showDistrictList(){
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
		Cursor curs;
		locationList.clear();
		List<GeoLocation> locations=new ArrayList<GeoLocation>();
		try{
			curs=db.rawQuery("select * from location",null);
			String count="Count: "+curs.getCount();
			//String count1=String.valueOf(curs.getCount());
			Toast.makeText(getApplicationContext(),count, Toast.LENGTH_LONG).show();
			if(curs.moveToFirst()){
			do{
					GeoLocation location=new GeoLocation();
					String district=curs.getString(curs.getColumnIndex("district"));
					location.setDistrict(district);
					double latitude=curs.getDouble(curs.getColumnIndex("latitude"));
					location.setLatitude(latitude);
					double longitude=curs.getDouble(curs.getColumnIndex("longitude"));
					location.setLongitude(longitude);
					String zone=curs.getString(curs.getColumnIndex("zone"));
					location.setZone(zone);
					locations.add(location);
					
			}while(curs.moveToNext());
			}
			}catch (SQLException e) {
				// TODO: handle exception
				Toast.makeText(this,"SQL ERROR", Toast.LENGTH_LONG).show();
				Log.d("CCR", "SQL ERROR");
			}
		for(GeoLocation location:locations){
			locationList.add(location.toString());
		}
		myListAdapdter=new MyListAdapdter(this,locationList);
		listView=(ListView)findViewById(R.id.listView1);
		listView.setAdapter(myListAdapdter);
		listView.setTextFilterEnabled(true);
		listView.setBackgroundColor(Color.GRAY);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

	}
	void backUpDatabase(){
		File sd=Environment.getExternalStorageDirectory();
		File data=Environment.getDataDirectory();
		if(sd.canWrite()){
			String currentDBPath="/data/"+"com.itsmeccr.emergencynepal"+"/databases/"+DataBaseHelper.DB_NAME;
			String backupDBPath=DataBaseHelper.DB_NAME;
			File currentDB=new File(data, currentDBPath);
			File  backupDB=new File(sd, backupDBPath);
			try {
				FileChannel src=new FileInputStream(currentDB).getChannel();
				FileChannel dst=new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();
				Toast.makeText(getApplicationContext(), backupDB.toString(), Toast.LENGTH_LONG).show();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		
		super.onStart();
		//showDistrictList();
		
		}
}
