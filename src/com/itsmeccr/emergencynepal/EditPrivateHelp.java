package com.itsmeccr.emergencynepal;

import java.util.ArrayList;
import java.util.List;

import com.itsmeccr.models.PrivateHelp;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class EditPrivateHelp extends Activity {
	ListView listView;
	private List<PrivateHelp> privateHelps=new ArrayList<PrivateHelp>();
	private SQLiteDatabase db=null;
	private DataBaseHelper databaseHelper;
	private MyListAdapdter myListAdapdter;
	private int id;
	private List<String> helpList=new ArrayList<String>();
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if(db!=null)
			db.close();
		databaseHelper.close();
		super.onDestroy();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_private_help);
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
		
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Cursor curs;
		privateHelps=new ArrayList<PrivateHelp>();
		helpList.clear();
		try{
			curs=db.rawQuery("select * from privatehelp",null);
			
			//String count1=String.valueOf(curs.getCount());
			//Toast.makeText(this,count1, Toast.LENGTH_LONG).show();
			if(curs.moveToFirst()){
			do{
					PrivateHelp privateHelp=new PrivateHelp();
					String name=curs.getString(curs.getColumnIndex("name"));
					privateHelp.setName(name);
					String mobile=curs.getString(curs.getColumnIndex("mobile"));
					privateHelp.setMobile(mobile);
					String address=curs.getString(curs.getColumnIndex("address"));
					privateHelp.setAddress(address);
					String email=curs.getString(curs.getColumnIndex("email"));
					privateHelp.setEmail(email);
					String relationship=curs.getString(curs.getColumnIndex("relationship"));
					privateHelp.setRelationship(relationship);
					id=curs.getInt(curs.getColumnIndex("_id"));
					privateHelps.add(privateHelp);
					
			}while(curs.moveToNext());
			}
			}catch (SQLException e) {
				// TODO: handle exception
				Log.d("CCR", "SQL ERROR");
			}
		for(PrivateHelp privateHelp:privateHelps){
			helpList.add(privateHelp.toString());
		}
		myListAdapdter=new MyListAdapdter(this,helpList);
		listView=(ListView)findViewById(R.id.listView1);
		listView.setAdapter(myListAdapdter);
		listView.setTextFilterEnabled(true);
		listView.setBackgroundColor(Color.GRAY);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Clicked on Item "+id, Toast.LENGTH_LONG).show();
				PrivateHelp oldHelp=privateHelps.get(pos);
				Intent intent=new Intent(getApplicationContext(),AddPrivateHelp.class);
				intent.putExtra("Task", "edit");
				intent.putExtras(oldHelp.toBundle());
				intent.putExtra("position", id);
				startActivity(intent);
				
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_edit_private_help, menu);
		return true;
	}

}
