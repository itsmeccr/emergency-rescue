package com.itsmeccr.emergencynepal;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ShowNotification extends Activity {
	private CharSequence title="Sample Title";//title to be shown in top
	private CharSequence contentTitle="Content Title";//title when viewed in notification bar
	private CharSequence contentText="This is the context text to be displayed";//content of notification bar
	/**
	 * These are setters and getters
	 * @param contentTitle
	 */
	public void setContentTitle(CharSequence contentTitle) {
		this.contentTitle = contentTitle;
	}
	public void setContentText(CharSequence contentText) {
		this.contentText = contentText;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	private NotificationManager mNManager;//Notification manager object
	private static final int NOTIFY_ID=1100;//Notificaiton ID
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		String ns = Context.NOTIFICATION_SERVICE; //string to initialize notification
		mNManager = (NotificationManager) getSystemService(ns);	//notification initialization
		Intent i=getIntent(); //getting the intent that called this activity
		this.title=i.getCharSequenceExtra("title");//getting the argument of the activity
		this.contentText=i.getCharSequenceExtra("contentText");//getting the argument of the activity
		this.contentTitle=i.getCharSequenceExtra("contentTitle");
		final Notification msg = new Notification(R.drawable.ic_launcher,
		title,
		System.currentTimeMillis());
		Context context = getApplicationContext();
		Intent targetIntent=new Intent(this,SetAddressManually.class);
		PendingIntent intent=PendingIntent.getActivity(ShowNotification.this, 0,targetIntent,Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		msg.defaults |= Notification.DEFAULT_SOUND;
		msg.flags |= Notification.FLAG_AUTO_CANCEL;
		msg.setLatestEventInfo(context, contentTitle, contentText, intent);
		mNManager.notify(NOTIFY_ID, msg);
		finish();
	}
	
	
}
