package com.itsmeccr.emergencynepal;





import java.io.File;

import com.itsmeccr.models.District;
import com.itsmeccr.models.ManualAddress;
import com.itsmeccr.models.Zone;

import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ContactsFragment extends Fragment {
View rootView;
public TextView userLocation;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//Toast.makeText(getActivity(), "On Create", Toast.LENGTH_LONG).show();
		
	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_emergency_contacts,
				container, false);
		/*Bundle args=getArguments();
		String location=args.getString("location");
		userLocation=(TextView)rootView.findViewById(R.id.myLocation);
		userLocation.setText(location);*/
		ImageButton callEmergencyButton = (ImageButton) rootView
				.findViewById(R.id.imageButton1);
		callEmergencyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
				dialog.setMessage("100 is Emergency Number.Are you sure you want to Call?");
				dialog.setButton(Dialog.BUTTON_POSITIVE, "Call", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						try{
							Intent callIntent=new Intent(Intent.ACTION_CALL);
							callIntent.setData(Uri.parse("tel:100"));
							startActivity(callIntent);
						}catch(ActivityNotFoundException e){
							Log.e("Calling a Phone Number","Call failed",e);
						}
					}
				});	
				dialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});	
				dialog.show();
			}
		});
		Button ambulance=(Button)rootView.findViewById(R.id.button3);
		ambulance.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent intent=new Intent(getActivity(),AmbulanceList.class);
				startActivity(intent);
				
			}
		});
		Button locationUpdate=(Button)rootView.findViewById(R.id.button8);
		locationUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getActivity(),SetAddressManually.class);
			startActivityForResult(intent, 1001);
				
			}
		});
		Button fire=(Button)rootView.findViewById(R.id.button4);
		fire.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			Intent intent=new Intent(getActivity(),FireList.class);
			startActivity(intent);
			}
		});
		Button policeStation=(Button)rootView.findViewById(R.id.police);
		policeStation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			Intent intent=new Intent(getActivity(),PoliceStationList.class);
			startActivity(intent);
			}
		});
		Button trafficPolice=(Button)rootView.findViewById(R.id.traffic);
		trafficPolice.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(getActivity(),TrafficPoliceList.class);
				startActivity(intent);
				
			}
		});
		Button hospital=(Button)rootView.findViewById(R.id.hospital);
		hospital.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getActivity(),HospitalList.class);
				startActivity(intent);
			}
		});
		Button privateHelp=(Button)rootView.findViewById(R.id.btnprivate_help);
		privateHelp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			Intent intent=new Intent(getActivity(),PrivateHelpList.class);
			startActivity(intent);
			}
		});
		//
		updateLocation();
		//Toast.makeText(getActivity(), "On Create View Called", Toast.LENGTH_LONG).show();
		return rootView;
	}


	public void updateLocation(){
		ManualAddress userAddress=new ManualAddress();
		Log.d("CCR","here i am0");
		
		Log.d("CCR","here i am1");
		String fileName="/data/data/com.itsmeccr.emergencynepal/shared_prefs/myAddressPreference.xml";
		//Toast.makeText(this, fileName, Toast.LENGTH_LONG).show();
		File f=new File(fileName);
		if(f.exists()){
		SharedPreferences prefs=this.getActivity().getSharedPreferences("myAddressPreference",Context.MODE_PRIVATE);
		
		 String zone=prefs.getString("zone", "");
		
		 String district=prefs.getString("district", "");
		 
		 String mun=prefs.getString("mun", "");
		
		 String locality=prefs.getString("locality", "");
		
		userAddress.setZone(Zone.valueOf(zone));
		
		userAddress.setDistrict(District.valueOf(district));
		userAddress.setMun(mun);
		userAddress.setLocality(locality);
		
		//Toast alert=Toast.makeText(getActivity(), "Your Location is set to "+userAddress.toString(), Toast.LENGTH_LONG);
		//alert.show();
		Log.d("CCR","here i am2");
		userLocation=(TextView) rootView.findViewById(R.id.myLocation);
		userLocation.setText(userAddress.toString());
		//return userAddress.toString();
		}
	}
		
}
	