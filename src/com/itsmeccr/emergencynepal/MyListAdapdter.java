package com.itsmeccr.emergencynepal;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyListAdapdter extends ArrayAdapter<String> {
	private final Activity context;
	private final List<String> lists;
	
	static class ViewHolder{
		public TextView text;
	}

	public MyListAdapdter(Context context,
			List<String> objects) {
		super(context, R.layout.my_list_even_row_layout, objects);
		// TODO Auto-generated constructor stub
		this.context=(Activity) context;
		lists=objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView=convertView;
		//reuse view
		//if(rowView==null){
			LayoutInflater inflater=context.getLayoutInflater();
			if(position%2!=0)
			rowView=inflater.inflate(R.layout.my_list_even_row_layout, null);
			else
			rowView=inflater.inflate(R.layout.my_list_odd_row_layout, null);
			//Configure ViewHolder
			ViewHolder viewHolder=new ViewHolder();
			viewHolder.text=(TextView)rowView.findViewById(R.id.textView1);
			rowView.setTag(viewHolder);
	//}
		//fill data
		ViewHolder holder=(ViewHolder)rowView.getTag();
		String s=lists.get(position);
		holder.text.setText(s);
		return rowView;
	}
	

}
