package com.itsmeccr.emergencynepal;

import java.util.ArrayList;
import java.util.List;

import com.itsmeccr.models.District;
import com.itsmeccr.models.ManualAddress;
import com.itsmeccr.models.TrafficPolice;
import com.itsmeccr.models.Zone;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class TrafficPoliceList extends ListActivity {
	private List<TrafficPolice> trafficPolices=new ArrayList<TrafficPolice>();
	private SQLiteDatabase db=null;
	private DataBaseHelper databaseHelper;
	private View header;
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if(db!=null)
			db.close();
		databaseHelper.close();
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		LayoutInflater inflator=getLayoutInflater();
		header=inflator.inflate(R.layout.activity_traffic_police, null);
		getListView().addHeaderView(header);
		ImageButton emergencyCall=(ImageButton)header.findViewById(R.id.call102);
		emergencyCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final AlertDialog dialog = new AlertDialog.Builder(TrafficPoliceList.this).create();
				dialog.setMessage("Are you sure you want to make Call to 103 ?");
				dialog.setButton(Dialog.BUTTON_POSITIVE, "Call", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						try{
							Intent callIntent=new Intent(Intent.ACTION_CALL);
							callIntent.setData(Uri.parse("tel:103"));
							startActivity(callIntent);
						}catch(ActivityNotFoundException e){
							Log.e("Calling a Phone Number","Call failed",e);
						}
					}
				});	
				dialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});	
				dialog.show();
			}
			
		});
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
		trafficPolices=new ArrayList<TrafficPolice>();
		//db.execSQL("delete from trafficPolice where phone='234567'");
		Cursor curs=db.rawQuery("select * from trafficpolice",null);
		//String count1=String.valueOf(curs.getCount());
		//Toast.makeText(this,count1, Toast.LENGTH_LONG).show();
		if(curs.getCount()==0){
			TextView tv=(TextView)header.findViewById(R.id.notFound);
			tv.setVisibility(View.VISIBLE);
			TextView tv1=(TextView)header.findViewById(R.id.head);
			tv1.setVisibility(View.GONE);
		}
		if(curs.moveToFirst()){
			
		
		do{
				ManualAddress address=new ManualAddress();
				TrafficPolice trafficPolice=new TrafficPolice();
				String name=curs.getString(curs.getColumnIndex("name"));
				trafficPolice.setName(name);
				String phone=curs.getString(curs.getColumnIndex("mobile"));
				trafficPolice.setPhone(phone);
				phone=curs.getString(curs.getColumnIndex("phone"));
				trafficPolice.setPhone(phone);
				String district=curs.getString(curs.getColumnIndex("district"));
				District dist=District.valueOf(district);
				address.setDistrict(dist);
				address.setZone(Zone.valueOf(dist.getZone()));
				String mun=curs.getString(curs.getColumnIndex("mun"));
				String locality=curs.getString(curs.getColumnIndex("locality"));
				address.setMun(mun);
				address.setLocality(locality);
				trafficPolice.setAddress(address);
				trafficPolices.add(trafficPolice);
		}while(curs.moveToNext());
		}
		 final List<String> ACTIVITY_CHOICES=new ArrayList<String>();
			for(TrafficPolice trafficPolice:trafficPolices)
			ACTIVITY_CHOICES.add(trafficPolice.toString());
			/*setListAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, ACTIVITY_CHOICES));*/
			setListAdapter(new MyListAdapdter(this,ACTIVITY_CHOICES));
					getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
					getListView().setTextFilterEnabled(true);
					getListView().setOnItemClickListener(new OnItemClickListener()
					{
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
						 final List<String> phones=trafficPolices.get(arg2-1).getPhone();
						 AlertDialog dialog = new AlertDialog.Builder(TrafficPoliceList.this).create();
							dialog.setMessage("Please Choose Phone Number");
							dialog.setButton(DialogInterface.BUTTON_NEUTRAL, phones.get(0), new DialogInterface.OnClickListener() {							
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									try{
										Intent callIntent=new Intent(Intent.ACTION_CALL);
										callIntent.setData(Uri.parse("tel:"+phones.get(0)));
										startActivity(callIntent);
									}catch(ActivityNotFoundException e){
										Log.e("Calling a Phone Number","Call failed",e);
									}
									
								}
							});
							if(phones.size()==2)
							dialog.setButton(DialogInterface.BUTTON_POSITIVE, phones.get(1), new DialogInterface.OnClickListener() {							
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									// TODO Auto-generated method stub
									try{
										Intent callIntent=new Intent(Intent.ACTION_CALL);
										callIntent.setData(Uri.parse("tel:"+phones.get(1)));
										startActivity(callIntent);
									}catch(ActivityNotFoundException e){
										Log.e("Calling a Phone Number","Call failed",e);
									}
									
								}
							});
							
							dialog.show();
						
								
					}
					});
		
		
	}

}
