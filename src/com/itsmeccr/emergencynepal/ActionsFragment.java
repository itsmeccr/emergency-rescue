package com.itsmeccr.emergencynepal;


import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class ActionsFragment extends Fragment {
	public ActionsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_emergency_actions,
				container, false);
		ImageButton givingCPR=(ImageButton)rootView.findViewById(R.id.imageButton1);
		givingCPR.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getActivity(),GivingCpr.class);
				startActivity(intent);
			}
		});
		return rootView;
	}

}
