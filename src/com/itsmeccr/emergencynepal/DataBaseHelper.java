package com.itsmeccr.emergencynepal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;


public class DataBaseHelper extends SQLiteOpenHelper {

	

	public static String DB_PATH=Environment.getDataDirectory()+"/data/"+"com.itsmeccr.emergencynepal"+"/databases/";
	public static String DB_NAME="emergency.db";
	public static int dbVersion;
	private SQLiteDatabase myDataBase;
	private final Context myContext;
public DataBaseHelper(Context context) {
		// TODO Auto-generated constructor stub
		super(context,DB_NAME, null,MainActivity.dbVersion);
		this.myContext=context;
		
	}

public void createDataBase() throws IOException{
		this.getReadableDatabase();
		try{
			copyDataBase();	
		}catch(IOException e){
			throw new Error("Error copying database");		
	}	
}


@SuppressWarnings("finally")
public boolean checkDataBase(){
	SQLiteDatabase checkDB=null;
	try{
		String myPath=DB_PATH+DB_NAME;
		checkDB=SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
	}catch(SQLiteException e){
		//database does not exist
		Log.d("CCR", "Database doesnot exist");
	}finally{
		Log.d("CCR", "Reached Here");
	return checkDB!=null?true:false;
	}	
}
private void copyDataBase() throws IOException{
	//opening local db as input
	InputStream myInput=myContext.getAssets().open(DB_NAME);
	//path to empty database
	String outFileName=DB_PATH+DB_NAME;
	//Open empty database as output stream
	Log.d("CCR", "Reached Here1");
	OutputStream myOutput=new FileOutputStream(outFileName);
	Log.d("CCR", "Reached Here2");
	//transfer the bytes from input file to outfile size
	byte[] buffer=new byte[1024];
	int length;
	while((length=myInput.read(buffer))>0){
		myOutput.write(buffer, 0, length);
	}
	myOutput.flush();
	myOutput.close();
	myInput.close();
}
public SQLiteDatabase openDataBase()throws SQLException{
	//open the database
	Log.d("CCR","Reached opendatabase");
	String myPath=DB_PATH+DB_NAME;
	myDataBase=SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
	return myDataBase;
}
@Override
public synchronized void close(){
	if(myDataBase!=null){
		myDataBase.close();
	}
	super.close();
}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}
	public void upgradeDataBase(){
		File myInput=new File(DB_PATH+DB_NAME);
		myInput.delete();
		this.getReadableDatabase();
		try {
			copyDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
