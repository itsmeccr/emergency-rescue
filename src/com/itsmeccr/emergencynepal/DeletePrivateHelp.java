package com.itsmeccr.emergencynepal;

import java.util.ArrayList;
import java.util.List;

import com.itsmeccr.models.PrivateHelp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DeletePrivateHelp extends Activity {
	private SQLiteDatabase db=null;
	private DataBaseHelper databaseHelper;
	//private PrivateHelp privateHelp;
	private MyListAdapdter myListAdapdter;
	private List<PrivateHelp> privateHelps=new ArrayList<PrivateHelp>();
	List<String> helpList=new ArrayList<String>();
	ListView listView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.private_help_delete1);
		databaseHelper=new DataBaseHelper(getApplicationContext());
		db=databaseHelper.openDataBase();
		ImageButton deleteAll=(ImageButton)findViewById(R.id.imageButton1);
		deleteAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				AlertDialog dialog = new AlertDialog.Builder(DeletePrivateHelp.this).create();
				dialog.setMessage("Are you sure you want to delete ALL contacts. You Cannot Recover Later?");
				dialog.setButton(DialogInterface.BUTTON_POSITIVE,"Confirm", new DialogInterface.OnClickListener() {							
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						
							try{
								db.execSQL("delete from privatehelp where 1");
								Toast.makeText(getApplicationContext(), "All Contacts Deleted", Toast.LENGTH_SHORT).show();
								finish();
							}catch(SQLException e){
							Toast.makeText(getApplicationContext(), "Failed to perform operation", Toast.LENGTH_SHORT).show();
							}
					}
				});
				dialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener() {							
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						Toast.makeText(getBaseContext(), "Delete Cancelled", Toast.LENGTH_SHORT).show();
						finish();
					}
				});
				
				dialog.show();
				
				
			}
				/*try{
					db.execSQL("delete from private help where name=");
				}catch(SQLException e){
				Toast.makeText(getApplicationContext(), "Failed to perform operation", Toast.LENGTH_SHORT).show();
				}
			}*/
		});
		privateHelps=new ArrayList<PrivateHelp>();
		helpList.clear();
		Cursor curs;
		try{
			curs=db.rawQuery("select * from privatehelp",null);
			
			//String count1=String.valueOf(curs.getCount());
			//Toast.makeText(this,count1, Toast.LENGTH_LONG).show();
			if(curs.moveToFirst()){
				
			
			do{
					PrivateHelp privateHelp=new PrivateHelp();
					String name=curs.getString(curs.getColumnIndex("name"));
					privateHelp.setName(name);
					String mobile=curs.getString(curs.getColumnIndex("mobile"));
					privateHelp.setMobile(mobile);
					String address=curs.getString(curs.getColumnIndex("address"));
					privateHelp.setAddress(address);
					String email=curs.getString(curs.getColumnIndex("email"));
					privateHelp.setEmail(email);
					String relationship=curs.getString(curs.getColumnIndex("relationship"));
					privateHelp.setRelationship(relationship);
					privateHelps.add(privateHelp);
			}while(curs.moveToNext());
			}
			}catch (SQLException e) {
				// TODO: handle exception
				Log.d("CCR", "SQL ERROR");
			}
		for(PrivateHelp privateHelp:privateHelps){
			helpList.add(privateHelp.toString());
		}
		myListAdapdter=new MyListAdapdter(this,helpList);
		listView=(ListView)findViewById(R.id.listView1);
		listView.setAdapter(myListAdapdter);
		listView.setTextFilterEnabled(true);
		listView.setBackgroundColor(Color.GRAY);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Clicked on Item "+pos, Toast.LENGTH_LONG).show();
				final PrivateHelp help=privateHelps.get(pos);
				AlertDialog dialog = new AlertDialog.Builder(DeletePrivateHelp.this).create();
				dialog.setMessage("Are you sure you want to delete this contact?");
				dialog.setButton(DialogInterface.BUTTON_POSITIVE,"Confirm", new DialogInterface.OnClickListener() {							
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						try{
							SQLiteStatement stm=db.compileStatement("delete from privatehelp where name=? and mobile=? and email=? and address=? and relationship=?");
							
							stm.bindAllArgsAsStrings(new String[]{help.getName(),help.getMobile(),help.getEmail(),help.getAddress(),help.getRelationship()});
							stm.execute();
							Toast.makeText(getBaseContext(), "Delete Success", Toast.LENGTH_SHORT).show();
							finish();
						}catch(SQLException e){
						Toast.makeText(getApplicationContext(), "Failed to perform operation", Toast.LENGTH_SHORT).show();
						}
					}
				});
				dialog.setButton(DialogInterface.BUTTON_NEGATIVE,"Cancel", new DialogInterface.OnClickListener() {							
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						Toast.makeText(getBaseContext(), "Delete Cancelled", Toast.LENGTH_SHORT).show();
						finish();
					}
				});
				
				dialog.show();
				
				
			}
		});
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if(db!=null)
			db.close();
		databaseHelper.close();
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	
}
