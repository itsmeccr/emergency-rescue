package com.itsmeccr.emergencynepal;



import java.io.File;
import java.io.IOException;


import com.itsmeccr.models.ManualAddress;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {
	
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	private String[] tabs={ "Emergency\nContacts", "Emergency\nActions",
	"Other\nServices" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
						
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(tabs[i])
					.setTabListener(this));
		}
		////////////////////////////My things to add on create///////////////////////////////
		manageDatabase();
		
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		checkLocation();
		mViewPager.getAdapter().notifyDataSetChanged();//This line updates the view
		/*ViewGroup container=(ViewGroup)findViewById(R.id.pager);
		android.app.Fragment currentFragment=getFragmentManager().findFragmentById(R.id.pager);
		FragmentTransaction fragTransaction=getFragmentManager().beginTransaction();
		fragTransaction.detach(currentFragment);
		fragTransaction.attach(currentFragment);
		fragTransaction.commit();*/
		
		//Toast.makeText(this, "On Refresh", Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub
			super.destroyItem(container, position, object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub
			return super.instantiateItem(container, position);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.

			switch (position) {

			case 0:
				ContactsFragment contactsFragment=new ContactsFragment();
				/*Bundle args=new Bundle();
				args.putString("location",userAddress.toString());
				contactsFragment.setArguments(args);*/
				return contactsFragment;
			case 1:
				return new ActionsFragment();
			case 2:
				return new ServicesFragment();
			}
			return null;

		}


		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase();
			case 1:
				return getString(R.string.title_section2).toUpperCase();
			case 2:
				return getString(R.string.title_section3).toUpperCase();
				default:
					return null;
			}
			
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO Auto-generated method stub
			//return super.getItemPosition(object);
			return POSITION_NONE;
		}
		
		
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////my own code goes here//////////////////////////////////////////////////////////
	public static int dbVersion=3;
	public void manageDatabase() {
		// TODO Auto-generated method stub
		DataBaseHelper databaseHelper=new DataBaseHelper(getApplicationContext());
		if(!databaseHelper.checkDataBase()){
			SharedPreferences prefs=getSharedPreferences("myDbVersion", MODE_PRIVATE);
			Editor mEditor=prefs.edit();
			mEditor.putInt("dbVersion", MainActivity.dbVersion);
			mEditor.commit();
			Toast.makeText(this, "Creating Database", Toast.LENGTH_LONG).show();
		try {
			databaseHelper.createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}else if(checkDbUpdate()){
			SharedPreferences prefs=getSharedPreferences("myDbVersion", MODE_PRIVATE);
			Editor mEditor=prefs.edit();
			mEditor.putInt("dbVersion", MainActivity.dbVersion);
			mEditor.commit();
			Toast.makeText(this, "Upgrading Database", Toast.LENGTH_LONG).show();
			databaseHelper.upgradeDataBase();
		}
		
	}
	public boolean checkDbUpdate(){
		SharedPreferences prefs=getSharedPreferences("myDbVersion", MODE_PRIVATE);
		if(prefs.contains("dbVersion")){
			int ver=prefs.getInt("dbVersion", 0);
			if(ver==MainActivity.dbVersion)
				return false;
			else
				return true;
		}else
			return false;
	
	}
	ManualAddress userAddress=new ManualAddress();
	public boolean checkLocation(){
		//TextView userLocation=(TextView) findViewById(R.id.myLocation);
		String fileName="/data/data/com.itsmeccr.emergencynepal/shared_prefs/myAddressPreference.xml";
		//Toast.makeText(this, fileName, Toast.LENGTH_LONG).show();
		File f=new File(fileName);
		if(f.exists())
		{
			return true;
		/*	
		Log.d("CCR","Inside check location");
		SharedPreferences prefs=getSharedPreferences("myAddressPreference",MODE_PRIVATE);
		
		 String zone=prefs.getString("zone", "");
		
		 String district=prefs.getString("district", "");
		 
		 String mun=prefs.getString("mun", "");
		
		 String locality=prefs.getString("locality", "");
		
		userAddress.setZone(Zone.valueOf(zone));
		
		userAddress.setDistrict(District.valueOf(district));
		userAddress.setMun(mun);
		userAddress.setLocality(locality);
		//ContactsFragment contactsFragment=new ContactsFragment();
		//contactsFragment.updateLocation();
		Toast alert=Toast.makeText(MainActivity.this, "Your Location is set to "+userAddress.toString(), Toast.LENGTH_LONG);
		alert.show();
		Log.d("CCR","here i am1");
		//userLocation.setText(userAddress.toString());
		Log.d("CCR","here i am2");
		//ImageView imgView=new ImageView(this);
		//imgView.setImageResource(R.drawable.ic_launcher);
		//alert.setView(imgView);
*/		
		 }else{
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setMessage("Your Location is not set");
		dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(MainActivity.this,SetAddressManually.class);
				startActivityForResult(intent, 1010);
			}
		});
		
		dialog.show();
		showNotification();
		return false;
		}
	}
	
	public void showNotification(){
		CharSequence title="Warning!!!Location is not Set";
		CharSequence contentText="Please set the location manually or using GPS for accurate results";
		CharSequence contentTitle="Location is not set";
		Intent notification=new Intent(MainActivity.this,ShowNotification.class);
		notification.putExtra("title",title);
		notification.putExtra("contentTitle", contentTitle);
		notification.putExtra("contentText",contentText);
		startActivity(notification);
		}

}



