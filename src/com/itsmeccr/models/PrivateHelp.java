package com.itsmeccr.models;

import android.os.Bundle;
import android.support.v4.os.ParcelableCompat;

public class PrivateHelp {
	private String name;
	private String address;
	private String mobile;
	private String email;
	private String relationship;
	private Bundle bundle;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public boolean setMobile(String mobile) {
		if(mobile.matches("\\d{10}")){
		this.mobile = mobile;
		return true;
		}else return false;
	}
	public String getEmail() {
		return email;
	}
	public boolean setEmail(String email) {
		if(email.matches("\\S+@\\S+\\.\\S+")){
		this.email = email;
		return true;
		}else return false;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	@Override
	public String toString(){
		String ret1="Name: "+getName()+"\n"+"Mobile No: "+getMobile()+"\nAddress: "+getAddress();
		String ret2="\nEmail: "+getEmail()+"\nRelationship: "+getRelationship();
		return ret1+ret2;
	}
	public Bundle toBundle(){
		bundle=new Bundle();
		bundle.putString("name", this.name);
		bundle.putString("address", this.address);
		bundle.putString("mobile", this.mobile);
		bundle.putString("email", this.email);
		bundle.putString("relationship", this.relationship);
	return bundle;	
	}
	public PrivateHelp fromBundle(Bundle bundle){
		PrivateHelp help=new PrivateHelp();
		help.name=bundle.getString("name");
		help.address=bundle.getString("address");
		help.mobile=bundle.getString("mobile");
		help.email=bundle.getString("email");
		help.relationship=bundle.getString("relationship");
		return help;
	}
	

}
