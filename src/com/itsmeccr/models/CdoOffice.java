package com.itsmeccr.models;

import java.util.ArrayList;
import java.util.List;

public class CdoOffice extends Contacts{
	private District district;
	private String headQuarter;
	List<String>phones=new ArrayList<String>();
	public CdoOffice() {
		super.name="District";
	}
	public void setName(String name){
		super.name=name;
		setDistrict();
			
	}
	
	public void setPhone(String phone){
		phones.add(phone);
		super.phone=phones;
	}
	public String getName(){
		return super.name;
	}
	
	public List<String> getPhone(){
		return super.phone;
	}
	private void setDistrict(){
		this.district=District.valueOf(this.name);
		setHeadQuarter();
	}
	private void setHeadQuarter(){
		this.headQuarter=district.getHq();
	}
	public District getDistrict(){
		return district;
	}
	public String getHeadQuarter(){
		return headQuarter;
	}
	@Override
	public String toString() {
		String retVal1="Name: "+getName()+"\nAddress: "+getHeadQuarter()+"\nPhone: ";
		String retVal2="";
		retVal2=getPhone()+"\n";	
		return retVal1+retVal2;
		
		
	}
	

}
