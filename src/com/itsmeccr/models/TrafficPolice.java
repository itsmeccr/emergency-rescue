package com.itsmeccr.models;

import java.util.ArrayList;
import java.util.List;

public class TrafficPolice extends Contacts{
	private String type;
	List<String>phones=new ArrayList<String>();

	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	public void setName(String name){
		super.name=name;
			
	}
	public void setAddress(ManualAddress address){
		super.address=address;
	}
	public void setPhone(String phone){
		phones.add(phone);
		super.phone=phones;
	}
	public String getName(){
		return super.name;
	}
	public ManualAddress getAddress(){
		return super.address;
	}
	public List<String> getPhone(){
		return super.phone;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String retVal1="Name: "+getName()+"\nAddress: "+getAddress().toString()+"\nPhone: ";
		String retVal2="";
		retVal2=getPhone()+"\nType:"+getType();	

		return retVal1+retVal2;
	}

}
