package com.itsmeccr.models;

public class GeoLocation {
	private String district;
	private double latitude;
	private double longitude;
	private String zone;
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String ret1="District: "+getDistrict()+"\nLatitude: "+getLatitude();
		String ret2="\nLongitude: "+getLongitude()+"\nZone: "+getZone();
		return ret1+ret2;
	}
	public double getDistance(GeoLocation location2){
		final double RADIUS=6371;
		double dLat=Math.toRadians(this.latitude-location2.latitude);
		double dLong=Math.toRadians(this.longitude-location2.longitude);
		double sum1=(Math.sin(dLat/2)*Math.sin(dLat/2));
		double sum2=(Math.cos(Math.toRadians(this.latitude))*Math.cos(Math.toRadians(location2.latitude))*Math.sin(dLong)/2)*Math.sin(dLong/2);
		double a=sum1+sum2;
		double c=2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		//System.out.println("Sin(30)="+a);
		return RADIUS*c;
	}
	

}
