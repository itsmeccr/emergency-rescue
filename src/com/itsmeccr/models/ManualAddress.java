package com.itsmeccr.models;

public class ManualAddress {
	private Zone zone;
	private District district;
	private String mun;
	private String locality;
	
	public ManualAddress(){
		mun="_";
		locality="_";
	}
	public Zone getZone() {
		return zone;
	}
	public void setZone(Zone zone) {
		this.zone = zone;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public String getMun() {
		return mun;
	}
	public void setMun(String mun) {
		this.mun = mun;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return locality+", "+mun+", "+district.name()+", "+zone.name();
	}
	
	
}
