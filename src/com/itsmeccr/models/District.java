package com.itsmeccr.models;

public enum District {
Jhapa("Mechi","Bhadrapur","023"),
Taplejung("Mechi","Taplejung","024"),
Panchthar("Mechi","Phidim","024"),
Illam("Mechi","Illam","027"),
Morang("Koshi","Biratnagar","021"),
Sunsari("Koshi","Inaruwa","025"),
Bhojpur("Koshi","Bhojpur","029"),
Terathum("Koshi","Myanglung","026"),
Dhankuta("Koshi","Dhankuta","026"),
Sankhuwasabha("Koshi","Khandbari","029"),
Saptari("Sagarmatha","Rajbiraj","031"),
Siraha("Sagarmatha","Siraha","033"),
Udayapur("Sagarmatha","Triyuga","035"),
Khotang("Sagarmatha","Diktel","036"),
Okhaldunga("Sagarmatha","Okhaldunga","037"),
Solukhumbu("Sagarmatha","Salleri","038"),
Dhanusa("Janakpur","Janakpur","041"),
Mahottari("Janakpur","Jaleswar","044"),
Sarlahi("Janakpur","Malangwa","046"),
Sindhuli("Janakpur","Kamalamai","047"),
Ramechhap("Janakpur","Manthali","048"),
Dolakha("Janakpur","Charikot","049"),
Bhaktapur("Bagmati","Bhaktapur","01"),
Dhading("Bagmati","Dhading Besi","010"),
Kathmandu("Bagmati","Kathmandu","01"),
Kavrepalanchok("Bagmati","Dhulikhel","011"),
Lalitpur("Bagmati","Lalitpur","01"),
Nuwakot("Bagmati","Bidur","010"),
Rasuwa("Bagmati","Dhunche","010"),
Sindhupalchok("Bagmati","Chautara","011"),
Bara("Narayani","Kalaiya","053"),
Parsa("Narayani","Birjung","051"),
Rautahat("Narayani","Gaur","055"),
Chitwan("Narayani","Bharatpur","056"),
Makawanpur("Narayani","Hetauda","057"),
Gorkha("Gandaki","Gorkha","064"),
Kaski("Gandaki","Pokhara","061"),
Lamjung("Gandaki","Besisahar","066"),
Syangja("Gandaki","Syangja","063"),
Tanahu("Gandaki","Byas","065"),
Manang("Gandaki","Chame","066"),
Kapilvastu("Lumbini","Kapilvastu","076"),
Nawalparasi("Lumbini","Parasi","078"),
Rupandehi("Lumbini","Siddharthanagar","071"),
Arghakhanchi("Lumbini","Sandhikharka","077"),
Gulmi("Lumbini","Tamghas","079"),
Palpa("Lumbini","Tansen","075"),
Baglung("Dhaulagiri","Baglung","068"),
Myagdi("Dhaulagiri","Beni","069"),
Parbat("Dhaulagiri","Kusma","067"),
Mustang("Dhaulagiri","Jomsom","069"),
Dang("Rapti","Tribhuvannagar","082"),
Pyuthan("Rapti","Pyuthan Khalanga","086"),
Rolpa("Rapti","Liwang","086"),
Rukum("Rapti","Musikot","088"),
Salyan("Rapti","Salyan Khalanga","088"),
Dolpa("Karnali","Dunai","087"),
Humla("Karnali","Simikot","019"),
Jumla("Karnali","Jumla Khalanga","087"),
Kalikot("Karnali","Manma","087"),
Mugu("Karnali","Gamgadhi","019"),
Banke("Bheri","Nepalganj","081"),
Bardiya("Bheri","Gulariya","084"),
Surkhet("Bheri","Birendranagar","083"),
Dailekh("Bheri","Narayan","089"),
Jajarkot("Bheri","Khalanga","089"),
Kailali("Seti","Dhangadi","091"),
Achham("Seti","Mangalsen","097"),
Doti("Seti","Dipayal","094"),
Bajhang("Seti","Chainpur","092"),
Bajura("Seti","Martadi","097"),
Kanchanpur("Mahakali","Bhim Datta","099"),
Dadeldhura("Mahakali","Dadeldhura","096"),
Baitadi("Mahakali","Gothalapani","095"),
Darchuala("Mahakali","Darchula","093");
private final String zone;
private final String hq;
private final String code;
District(String zone,String hq,String code){
	this.zone=zone;
	this.hq=hq;
	this.code=code;
}
public String getCode() {
	return code;
}
public String getZone() {
	return zone;
}
public String getHq() {
	return hq;
}



}
