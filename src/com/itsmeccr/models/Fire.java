package com.itsmeccr.models;

import java.util.ArrayList;
import java.util.List;

public class Fire extends Contacts{
	private int vehicleNo;
	List<String>phones=new ArrayList<String>();
	
	public Fire(){
		super.name="Fire Brigade";
	}
	
	public int getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(int vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public void setName(String name){
		super.name=name;
			
	}
	public void setAddress(ManualAddress address){
		super.address=address;
	}
	public void setPhone(String phone){
		phones.add(phone);
		super.phone=phones;
	}
	public String getName(){
		return super.name;
	}
	public ManualAddress getAddress(){
		return super.address;
	}
	public List<String> getPhone(){
		return super.phone;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String retVal1="Name: "+getName()+"\nAddress: "+getAddress().toString()+"\nPhone: ";
		String retVal2="";
		retVal2=getPhone()+"\n";	
		return retVal1+retVal2;
		
	}

}
