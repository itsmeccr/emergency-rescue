package com.itsmeccr.models;

public enum Zone {
	
Mechi(1,"Mechi","EDR"),
Koshi(2,"Koshi","EDR"),
Sagarmatha(3,"Sagarmatha","EDR"),
Janakpur(4,"Janakpur","CDR"),
Narayani(5,"Narayani","CDR"),
Bagmati(6,"Bagmati","CDR"),
Gandaki(7,"Gandaki","WDR"),
Dhaulagiri(8,"Dhaulagiri","WDR"),
Lumbini(9,"Lumbini","WDR"),
Rapti(10,"Rapti","MWDR"),
Bheri(11,"Bheri","MWDR"),
Karnali(12,"Karnali","MWDR"),
Seti(13,"Seti","FWDR"),
Mahakali(14,"Mahakali","FWDR");


private final String zoneName;
private final int serialNo;
private final String region;
Zone(int sn,String zone,String region){
	this.zoneName=zone;
	this.serialNo=sn;
	this.region=region;
}
public String getZoneName() {
	return zoneName;
}
public int getSerialNo() {
	return serialNo;
}
public String getRegion() {
	return region;
}
@Override
public String toString() {
	// TODO Auto-generated method stub
	return zoneName;
}


}