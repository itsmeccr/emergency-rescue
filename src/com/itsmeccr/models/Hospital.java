package com.itsmeccr.models;

import java.util.ArrayList;
import java.util.List;

public class Hospital extends Contacts {
	private String specialization;
	List<String>phones=new ArrayList<String>();
	public Hospital(){
		super.name="Hospital";
		specialization="General";
	}
	public String getSpecialization() {
		return specialization;
	}
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	public void setName(String name){
		super.name=name;
			
	}
	public void setAddress(ManualAddress address){
		super.address=address;
	}
	public void setPhone(String phone){
		phones.add(phone);
		super.phone=phones;
	}
	public String getName(){
		return super.name;
	}
	public ManualAddress getAddress(){
		return super.address;
	}
	public List<String> getPhone(){
		return super.phone;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String retVal1="Name: "+getName()+"\nAddress: "+getAddress().toString()+"\nPhone: ";
		String retVal2="";
		retVal2=getPhone()+"\nSpecialization:"+getSpecialization();	

		return retVal1+retVal2;
	}
	

}
