package com.itsmeccr.models;



import java.util.List;


public abstract class Contacts {
	protected String name;
	protected ManualAddress address;
	protected List<String> phone;
	public abstract String toString();	
}
